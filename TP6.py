import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Cargar Productos')
    print('2) Mostrar Productos')
    print('3) Mostrar Productos con stock [desde-hasta]')
    print('4) Modifica Stock con X cantidad para stock menor al valor Y')
    print('5) Eliminar Productos con stock 0 ')
    print('6) Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion

def validaPositivo(x):
    valida = False
    if x < 0:
        valida = True
    return valida

def leerPrecio():
    precio = float(input('Precio: '))
    while validaPositivo(precio):
        print('El Precio no puede ser negativo!!!')
        precio = float(input('Precio: '))
    return precio  

def leerStock():
    stock = int(input('Stock: '))
    while validaPositivo(stock):
        print('El Stock no puede ser negativo!!!')
        stock = int(input('Stock: '))
    return stock   

def mostrarProductos(diccionario):
    print('Listado de Productos')
    for clave, valor in diccionario.items():
        print(clave,valor)

def leerProductos():
    print('Cargar Lista de Productos')
    productos = {   25:['Lapiz', 35.0, 80],
                    15:['Goma', 21.0, 0],
                    36:['Lapicera', 45.5, 0],
                    12:['Regla 20cm', 66.5, 30]}
    continua ='s'
    while (continua == 's'):
        codigo = int(input('Ingrese Codigo: '))
        if codigo not in productos: 
            descripcion = input('Descripcion: ')
            precio = leerPrecio()
            stock = leerStock()
            productos[codigo] = [descripcion, precio, stock]
            print('Producto agregado correctamente')
        else:
            print('el producto ya existe')
        continua = input('Desea cargar otros Productos: s/n?')    
    return productos

def leerDesdeHasta():
    print('Stock desde:')
    stockdesde = leerStock()
    print('Stock Hasta:')
    stockHasta = leerStock()
    while stockHasta < stockdesde:
        print('Debe ingresar un Stock mayor a: ',stockdesde)
        stockHasta = leerStock()
    return stockdesde, stockHasta


def mostrarStock(productos, desde, hasta):
    print('Lista de Productos con stock entre [',desde, '-',hasta,']')
    for codigo, datos in productos.items():
        if (datos[2] >= desde and datos[2]<= hasta): 
            print(codigo, datos)

def mostrarStockDesdeHasta(productos):
        desde,hasta= leerDesdeHasta()
        mostrarStock(productos, desde, hasta)

#principal
opcion = 0
productos = {}
os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = leerProductos()
    elif opcion == 2:
        mostrarProductos(productos)
    elif opcion == 3:
        mostrarStockDesdeHasta(productos)
    #elif opcion == 4:
        #modificarStock(productos)
    #elif opcion == 5:
        #eliminar(productos)
    #elif (opcion == 6):        
        #salir()
    continuar()